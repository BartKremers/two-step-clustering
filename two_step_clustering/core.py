""" Implements data-density reduction, using the DBSCAN and k-means algorithms,
as described in the Master thesis of B.J.J. Kremers (TU/e) (with some updates)

Example command line call:
python <path-to-repo>/two_step_clustering_pkg/two_step_clustering.py --settings='settings.json' -vvvv

Usage:

'two_step_clustering' runs the reduction algorithm, including the DBSCAN and
k-means steps. This algorithm is as follows:
1) rescales data to unit variance and zero mean
2) initial DBSCAN to identify outliers (which are removed) and clusters (with initial_eps).
3) Clusters which pass size condition (min_cluster_size < size < RT, see explanation of
   settings below) are k-means reduced.
4) Then starts the reduction loop: in each iteration, the eps value is reduced by 0.05,
   DBSCAN is run to ID clusters, and these clusters are reduced if they pass the size
   condition. Exit conditions to this loop are (1) if DBSCAN finds no more clusters,
   (2) if min_eps is reached, or (3) if that iteration reduced all the remaining clusters.

'run_clustering' takes a path to a settings file, opens the requested dataset, and runs
the 'two_step_clustering' according to the settings. (Default settings are stored in the
'settings_DBMSND.json' file.) The settings include:

    "dims": list of dataset dimensions to be included in the reduction,
            both inputs and outputs
    "dataset_path": dataset to be reduced
    "output_name": where reduced results are stored
    "kmeans_dask": whether to use dask memory management module
    "initial_eps": initial radius passed to DBSCAN to determine clusters (default 1)
    "min_eps": stopping radius; reduction loop stops once this radius is reached
               (default 0.3)
    "eps_step": amount to step radius down by in each two-step-clustering iteration
                (default 0.05)
    "min_samples": DBSCAN parameter which determines which points are 'center points'
                   of a cluster (default 2)
    "min_cluster_size": passed to 'run_dbscan' (default 6)
    "reduction_threshold": passed to 'run_dbscan' (default 50)
            These 2 parameters bound which clusters are flagged for k-means clustering
            in that iteration. By default, clusters of size min_cluster_size < size < RT
            are flagged.
    "min_reduction": (default 20)
    "zeta": (default 0)
    "tau": (default 1000)
            These 3 parameters are used to calculate the k-value (how many points to
            replace 1 cluster with). The calculation is done in 'k_expression_bart'.


"""

""" 
TODO: perhaps add option for generating statistics,
the goal being to help tune the settings.

one row of stats for every iteration of two_step_clustering

1. number outliers (to lower, could lower min_samples, min_cluster_size,
   or eps_step)

2. number clusters immediately added to final set
2.1. % of all clusters found
3. number points from clusters immediately added to final set
3.1. % of all points in that iter
    (these stats determine the upper bound of actual reduction)
    (to lower all these, could lower min_cluster_size, lower eps_step,
    or increase min_samples)

4. minimum density_local (from run_kmeans) of all clusters tagged for reduction
5. maximum density_local "
    (these give an intuition of what to choose for the 3 k value settings:
    min_reduction, zeta, and tau. These stats can be used in conjunction with
    the function plot_kval_explore to determine the optimal settings.)

"""


import os
import sys
import re
import copy
import json
import math
import random
from pathlib import Path

import numpy as np
import pandas as pd
import xarray as xr
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import DBSCAN
from sklearn.neighbors import NearestNeighbors
from sklearn.cluster import KMeans
from sklearn.metrics import pairwise_distances_argmin_min
from typing import Any

import matplotlib as mpl
from matplotlib.pyplot import cm
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import itertools

dask_available = True
try:
    import dask_ml.cluster
except ImportError:
    dask_available = False

qlknn_available = True
try:
    from qlknn.dataset.data_io import load_from_store
except ImportError:
    qlknn_available = False

from . import setup_logging
logger = setup_logging.logging.getLogger("two-step-clustering")


def compute_n_ball_volume(radius, ndims=3):
    """Computes volume of an n-dimensional sphere

    Args:
        radius (float): distance from center to edge of the n-dimensional sphere
        ndims (int, optional): number of dimensions which sphere resides in. Defaults to 3.

    Returns:
        [float]: volume of n-dimensional sphere
    """
    base = ndims % 2
    volume = 1.0 if base == 0 else 2.0 * float(radius)
    level = base + 2
    while level <= ndims:
        volume *= 2.0 * np.pi * float(radius)**2.0 / float(level)
        level += 2
    return volume


def k_expression_bart(density, npts, rmin, tau=0.0, zeta=0.0):
    """Determines k value for k-means reduction. With zeta=0, this is simply
    npts/rmin = npts/settings['min_reduction'].
    zeta=1 gives full weight to the density-weighted expression. In this
    regime, the more dense clusters get reduced more (return a lower k value)
    than less dense clusters.

    (To graphically see dependencies in this expression: use the 'plot_kval_explore'
    function)

    Args:
        density (int): density of cluster
        npts (int): number of points in given cluster
        rmin (int): base reduction ratio, equal to settings['base_reduction_ratio']
        tau (float, optional): density weighting factor in exponential. Defaults to 0.0.
        zeta (float, optional): See above. Defaults to 0.0.

    Returns:
        [int]: k value for k-means clustering
    """
    k_base = float(npts) * float(rmin)
    k_adjustment = np.exp(-float(tau) * float(density))
    k_cluster = int(
        np.floor(
            k_base * (1.0 - float(zeta) * (1.0 - k_adjustment))
        )
    )
    if k_cluster < 1:
        k_cluster = 1
    return k_cluster


def plot_kval_explore(density=np.logspace(np.log10(200), np.log10(10000), 5),
        min_cluster_size=6, reduction_threshold=100, base_ratio=1.0,
        zeta=1, tau=5e-4, savefig=False):

    print(repr({'density bounds': (min(density), max(density)),
                'min_cluster_size': min_cluster_size,
                'reduction_threshold': reduction_threshold,
                'base_reduction_ratio': base_ratio,
                'zeta': zeta,
                'tau': tau}))

    npts = np.linspace(
        min_cluster_size, reduction_threshold,
        reduction_threshold-min_cluster_size+1
    )

    colors = itertools.cycle(plt.cm.tab20(np.linspace(0, 1, len(density) if len(density) <= 20 else 20)))

    fig, ax = plt.subplots()

    for d in density:
        res = np.zeros_like(npts)
        for i, n in enumerate(npts):
            res[i] = k_expression_bart(d, n, base_ratio, tau, zeta)

        ax.plot(npts, res, c=next(colors), label='d='+str(round(d, 2)))

    ax.legend()
    ax.set_xlabel('npts')
    ax.set_ylabel('k')
    fig.tight_layout()
    if savefig:
        pname = 'kval_rmin{:.0f}_tau{:.0e}_zeta{:.0e}.png'.format(1.0 / base_ratio, tau, zeta)
        fig.savefig(pname, format='png', bbox_inches='tight')
    else:
        plt.show()
    plt.close(fig)


def rescale_data(dataframe):

    data = dataframe.to_numpy()

    ### Re-scale data to unit variance and zero mean
    scaler = StandardScaler()
    scaler.fit(data)
    data_sigma = scaler.scale_
    data_stdv = scaler.transform(data)
    data_mean = scaler.mean_
    data_scaled = np.zeros(data.shape)
    for i in range(0, data_scaled.shape[1]):
        # Seems unnecessarily complex but ok
        data_scaled[:, i] = 1 / (1 / data_sigma[i]) * data_stdv[:, i]

    return pd.DataFrame(data_scaled, index=dataframe.index, columns=dataframe.columns)


def run_dbscan(dataframe, eps=1.0, reduce_next=False, **kwargs):
    """reduce_next: whether to also reduce clusters with size > reduction_threshold"""

    reduction_threshold = kwargs.get("reduction_threshold", None)
    #min_samples = kwargs.get("min_samples", 1)
    min_cluster_size = kwargs.get("min_cluster_size", 2)
    if min_cluster_size <= 1:
        min_cluster_size = 2

    data = dataframe.to_numpy()

    # NOTE: Data may be scaled, eps should be adjusted relative to data scale!
    db = DBSCAN(eps=eps, min_samples=min_cluster_size).fit(data)
    logger.info("DBSCAN step complete!")
    labels = db.labels_
    # Mask for detecting outliers (-1 in labels)
    outliers_samples_mask = labels == -1
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
    n_outliers_ = list(labels).count(-1)
    logger.info("Number of clusters found: %d" % n_clusters_)
    logger.info("Number of outliers points: %d" % n_outliers_)

    indices = None
    if n_clusters_ != 0:
        # Cluster number stored in 'unique' and number of points stored in 'counts'
        unique, counts = np.unique(labels, return_counts=True)
        sorter = np.argsort(-counts)  # Sort by number of points in ascending order
        counts = counts[sorter]
        unique = unique[sorter]
        # Mask for DBSCAN outputs which are not outliers
        remove_outliers_mask = unique != -1
        # Mask for clusters tagged for reduction
        reduction_cluster_mask = counts >= min_cluster_size
        # Mask for clusters which are larger than requested reduction threshold (RT)
        counts_mask = counts > reduction_threshold

        # Outliers removed
        cluster_indices_toss = unique[~remove_outliers_mask]
        # Outliers and clusters with npts < RT removed
        cluster_indices_next = unique[
            reduction_cluster_mask & remove_outliers_mask & counts_mask
        ]
        # Outliers and clusters with npts > RT and npts < min_size removed
        cluster_indices_reduce = unique[
            reduction_cluster_mask & remove_outliers_mask & ~counts_mask
        ]
        # Outliers and clusters with npts > min_size removed, this should always be zero
        cluster_indices_keep = unique[~reduction_cluster_mask & remove_outliers_mask]

        indices = {
            "toss": dataframe.loc[
                np.isin(labels, cluster_indices_toss).tolist()
            ].index.to_list(),
            "next": dataframe.loc[
                np.isin(labels, cluster_indices_next).tolist()
            ].index.to_list(),
            "reduce": [],
            "keep": dataframe.loc[
                np.isin(labels, cluster_indices_keep).tolist()
            ].index.to_list(),
        }
        for k in range(0, len(cluster_indices_reduce)):  # Loop over clusters
            cluster_mask = labels == cluster_indices_reduce[k]
            indices["reduce"].append(dataframe.loc[cluster_mask].index.to_list())
        if reduce_next:
            for k in range(0, len(cluster_indices_next)):
                cluster_mask = labels == cluster_indices_next[k]
                indices["reduce"].append(dataframe.loc[cluster_mask].index.to_list())

    return indices, labels


def run_kmeans(dataframe, radius=1.0, **kwargs):

    num_dim = kwargs.get("num_dim", 1)
    base_ratio = kwargs.get("base_reduction_ratio", 1.0)
    tau = kwargs.get("tau", 1.0)
    zeta = kwargs.get("zeta", 1.0)
    dask_flag = kwargs.get("kmeans_dask", False)
    verbose = kwargs.get("verbose", 0)

    noise_flag = kwargs.get("noise_flag", False)
    noise_ratio = kwargs.get("noise_reduction_ratio", None)

    data = dataframe.to_numpy()
    index = dataframe.index.to_list()

    # NOTE: Data may be scaled, radius should be adjusted relative to data scale!
    neigh = NearestNeighbors(radius=radius)
    neigh.fit(data)
    distance_neighbours, index_neighbours = neigh.radius_neighbors()
    point_density = np.array([])
    for k in range(data.shape[0]):  # Loop over the points within a given cluster
        volume = compute_n_ball_volume(radius, num_dim)
        result = index_neighbours[k].shape[0] / volume
        point_density = np.append(point_density, result)
    number_points = data.shape[0]
    density_local = np.nanmax(point_density)

    if noise_flag and noise_ratio is not None:
        base_ratio = noise_ratio
        if verbose > 1:
            logger.info(
                "    Noise data   : %d points." % (number_points)
            )
    k_cluster = k_expression_bart(density_local, number_points, base_ratio, tau=tau, zeta=zeta)
    if verbose > 2:
        logger.info(
            "    %14s density = %10.4f" % ("", density_local)
        )
        logger.info(
            "    %14s k = %d" % ("", k_cluster)
        )
    if not (dask_available and dask_flag):
        kmeans = KMeans(n_clusters=k_cluster, random_state=0).fit(data)
    else:
        kmeans = dask_ml.cluster.KMeans(n_clusters=k_cluster, random_state=0).fit(data)
    cluster_labels = kmeans.labels_
    cluster_center = kmeans.cluster_centers_

    # Data point closest to kmean centroid is flagged for keeping
    closest, _ = pairwise_distances_argmin_min(cluster_center, data)
    result_indices = dataframe.iloc[closest].index
    # index_of_closest = int(result_indices[0]) if len(result_indices) > 0 else None
    indices_of_closest = result_indices if len(result_indices) > 0 else None

    return indices_of_closest


def plot_clusters_3d(data, labels, plot_vars=None, savefig=True, figname='dummy.png'):

    if plot_vars == None and data.shape[1] != 3:
        logger.warning("Plotting only meant for 3D testsets (specify plot_variables)")
        return
    elif plot_vars == None and data.shape[1] == 3:
        plot_vars = list(data.columns)
    
    x, y, z = plot_vars[0], plot_vars[1], plot_vars[2]

    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)

    if n_clusters_ != 0:
        unique = np.unique(labels)

        colors = itertools.cycle(plt.cm.tab20(np.linspace(0, 1, len(unique) if len(unique) <= 20 else 20)))

        c_array = np.zeros((labels.shape[0], 4))
        for label in unique:
            if label == -1: # outliers colored black, RGBa
                c_array[labels == label, :] = np.array([0,0,0,1])
            else:
                c_array[labels == label, :] = next(colors)
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(data.loc[:, x], data.loc[:, y], data.loc[:, z], c=c_array, s=0.8**2)
        yticks = ax.get_yticks().round(2)
        ax.set_yticks(yticks) # prevent user warning for next line
        ax.set_yticklabels(yticks, va='bottom', ha='left') # so y labels align with ticks more prettily
        ax.set_xlabel(x)
        # ax.invert_xaxis()
        ax.set_ylabel(y)
        ax.set_zlabel(z)

        if savefig:
            fig.savefig(figname, format='png', bbox_inches='tight')
        else:
            plt.show()
        plt.close(fig)


def two_step_clustering(data, **settings):

    if not isinstance(data, pd.DataFrame):
        raise TypeError("DataFrame expected as input! Aborting!")

    ### Extract required settings from settings list
    num_dim = settings.get("num_dim", 1)
    reduction_threshold = settings.get("reduction_threshold", None)
    noise_ratio = settings.get("noise_reduction_ratio", 0.0)  # Reduction ratio applied to noise
    initial_eps = settings.get("initial_eps", 1.0)  # Initial value for scaling eps
    delta_eps = settings.get("delta_eps", 0.05)  # Iteration step size for scaling eps
    min_eps = settings.get("min_eps", 0.01)  # Minimum value for scaling eps
    verbose = settings.get("verbose", 0)
    fdebug = settings.get("debug_mode", False)
    plot = settings.get("plot", False)
    plot_vars = settings.get("plot_variables", None)

    ### Determine the maximum density resolvable based on inputs and settings
    if verbose > 0:
        logger.info("Dataset size: %d" % (len(data)))
        density_max = reduction_threshold / (min_eps ** num_dim)
        logger.info(
            "Maximum density resolvable: %f / %d-D unit volume" % (density_max, num_dim)
        )

    logger.info("Start scaling data")
    data_scaled = rescale_data(data)

    ### Initial DBSCAN, used to remove outliers => unclustered points in first DBSCAN iteration
    logger.info("Start initial DBSCAN-kmeans run")
    index_vectors, labels = run_dbscan(data_scaled, eps=1.0, **settings)

    if index_vectors is None:
        logger.info("No clusters discovered in initial pass, reconfigure settings.")
        return

    if plot and plot_vars is not None:
        plot_clusters_3d(data_scaled, labels, plot_vars=plot_vars, figname='initial.png')

    # Data to keep within final reduced list - only the initial pass throws away the noise
    index_keep = index_vectors["keep"]
    if verbose > 0:
        logger.info("  %d points passed to next iteration." % (len(index_vectors["next"])))
        logger.info("  %d points identified as noise (thrown)." % (len(index_vectors["toss"])))
    if verbose > 1:
        logger.info("  %d points inserted directly in final set." % (len(index_vectors["keep"])))
    # Apply kmeans to clusters tagged for reduction as is
    if len(index_vectors["reduce"]) != 0:
        if verbose > 1:
            logger.info("  %d clusters identified." % (len(index_vectors["reduce"])))
            np_clusters = 0
            for j in range(0, len(index_vectors["reduce"])):
                np_clusters += len(index_vectors["reduce"][j])
            logger.info("  %d points to be reduced in this iteration." % (np_clusters))
        for k in range(0, len(index_vectors["reduce"])):  # Loop over clusters
            if verbose > 2:
                logger.info("    Cluster %5d: %d points." % (k, len(index_vectors["reduce"][k])))
            data_cluster = data_scaled.loc[index_vectors["reduce"][k]]
            index_list = run_kmeans(data_cluster, radius=initial_eps, **settings)
            index_keep.extend(index_list)
    # Keep track of number of points in final dataset selected by this iteration
    iteration_counter = np.array([len(index_keep)])
    # Data to pass onto next iteration
    data_scaled = data_scaled.loc[index_vectors["next"]]

    logger.info("Start DBSCAN iterations")
    scaling_eps = initial_eps
    eps_counter = np.array([scaling_eps])
    ### TODO: This should be changed to a while loop
    max_iter = 10000 if not fdebug else 1
    for i in range(0, max_iter):
        # Perform secondary DBSCAN on the scaled data after removing outliers
        logger.info("DBSCAN iteration number: %d" % (i + 1))
        index_vectors, labels = run_dbscan(data_scaled, eps=scaling_eps, **settings)
        # Exit loop if all clusters tagged for reduction have been reduced
        if index_vectors is None:
            logger.info("No more clusters found by DBSCAN")
            break
        
        if plot and plot_vars is not None:
            plot_clusters_3d(data_scaled, labels, plot_vars=plot_vars, figname=f'iter{i+1}.png')

        # Data to keep within final reduced list - all loop iterations keep outliers
        if noise_ratio < 1.0 and noise_ratio > 0.0 and len(index_vectors["toss"]) > 0:
            noise_data = data_scaled.loc[index_vectors["toss"]]
            index_list = run_kmeans(noise_data, radius=scaling_eps, noise_flag=True, **settings)
            index_keep.extend(index_list)
        elif noise_ratio >= 1.0:
            index_keep.extend(index_vectors["toss"])
        index_keep.extend(index_vectors["keep"])
        if verbose > 0:
            logger.info("  %d points passed to next iteration." % (len(index_vectors["next"])))
        if verbose > 1:
            logger.info("  %d points identified as noise (kept)." % (len(index_vectors["toss"])))
            logger.info(
                "  %d points inserted directly in final set."
                % (len(index_vectors["toss"]) + len(index_vectors["keep"]))
            )
        # Apply kmeans to clusters tagged for reduction as is
        if len(index_vectors["reduce"]) != 0:
            if verbose > 1:
                logger.info("  %d clusters identified." % (len(index_vectors["reduce"])))
                np_clusters = 0
                for j in range(0, len(index_vectors["reduce"])):
                    np_clusters += len(index_vectors["reduce"][j])
                logger.info("  %d points to be reduced in this iteration." % (np_clusters))
            for k in range(0, len(index_vectors["reduce"])):  # Loop over clusters
                if verbose > 2:
                    logger.info(
                        "    Cluster %5d: %d points." % (k, len(index_vectors["reduce"][k]))
                    )
                data_cluster = data_scaled.loc[index_vectors["reduce"][k]]
                index_list = run_kmeans(data_cluster, radius=scaling_eps, **settings)
                index_keep.extend(index_list)
        # Keep track of number of points in final dataset selected by this iteration
        iteration_counter = np.hstack((iteration_counter, len(index_keep)))
        # Data to pass onto next iteration
        data_scaled = (
            data_scaled.loc[index_vectors["next"]]
            if len(index_vectors["next"]) != 0
            else pd.DataFrame()
        )
        logger.info(
            "Number of points to pass forward: %d" % (len(index_vectors["next"]))
        )

        if (scaling_eps - delta_eps) < min_eps:
            logger.info("Minimum radius is reached, stopping iterations")
            break
        if len(data_scaled) == 0:
            logger.info("No more large clusters to reduce, stopping iterations")
            break

        # Reducing the radius vector for each iteration
        scaling_eps = scaling_eps - delta_eps
        eps_counter = np.hstack((eps_counter, scaling_eps))  # Saving the new radius
        logger.info("New eps: %f." % (scaling_eps))

        if scaling_eps < min_eps:
            logger.info("Minimum radius is reached, stopping iterations")
            break

    # One final pass is done to reduce remaining data done at minimum radius, in case of minimum radius exit condition
    if len(data_scaled) != 0:
        logger.info("Start final DBSCAN-kmeans run")
        index_vectors, labels = run_dbscan(
            data_scaled, eps=min_eps, reduce_next=True, **settings
        )

        if plot and plot_vars is not None and index_vectors is not None:
            plot_clusters_3d(data_scaled, labels, plot_vars=plot_vars, figname=f'iter{i+2}-final.png')

        # Data to keep within final reduced list - all loop iterations keep outliers
        if noise_ratio < 1.0 and noise_ratio > 0.0 and len(index_vectors["toss"]) > 0:
            noise_data = data_scaled.loc[index_vectors["toss"]]
            index_list = run_kmeans(noise_data, radius=scaling_eps, noise_flag=True, **settings)
            index_keep.extend(index_list)
        elif noise_ratio >= 1.0:
            index_keep.extend(index_vectors["toss"])
        index_keep.extend(index_vectors["keep"])
        if verbose > 3:
            # These should be zero if printed
            logger.info("  %d points identified as noise (kept)." % (len(index_vectors["toss"])))
            logger.info("  %d points inserted directly in final set." % (len(index_vectors["toss"]) + len(index_vectors["keep"])))
        if verbose > 2:
            logger.info("  %d clusters identified." % (len(index_vectors["reduce"])))
        if verbose > 0:
            np_clusters = 0
            for j in range(0, len(index_vectors["reduce"])):
                np_clusters += len(index_vectors["reduce"][j])
            logger.info("  %d points to be reduced in the final iteration." % (np_clusters))
        for k in range(0, len(index_vectors["reduce"])):  # Loop over clusters
            if verbose > 0:
                logger.info("    Cluster %5d: %d points." % (k, len(index_vectors["reduce"][k])))
            data_cluster = data_scaled.loc[index_vectors["reduce"][k]]
            index_list = run_kmeans(data_cluster, radius=scaling_eps, **settings)
            index_keep.extend(index_list)
        iteration_counter = np.hstack((iteration_counter, len(index_keep)))

    data_final = data.loc[
        index_keep
    ]  # Output data is not ordered identically to input data
    logger.info("Final number of points: %d" % data_final.shape[0])
    # logger.info("size of the iteration counter: %d" % iteration_counter.shape[0])

    return data_final


def run_clustering(settingspath, verbose=0, fdebug=False, plot=False, plot_vars=None):

    spath = Path(settingspath)
    if not spath.is_file():
        raise IOError("Settings file %s was not found! Aborting!" % str(spath))

    ### Load settings
    logger.info("Reading JSON settings file")
    settings = dict()
    with open(str(spath.resolve())) as file_:
        settings = json.load(file_)
    settings["verbose"] = verbose
    settings["debug_mode"] = fdebug
    settings["plot"] = plot
    if plot and isinstance(plot_vars, (list, tuple)):
        settings["plot_variables"] = plot_vars
    if "plot_variables" in settings and len(plot_vars) != 3:
        raise Exception("3D plot variables {} incorrectly parsed".format(plot_vars))
    if verbose > 1 or fdebug:
        logger.info(repr(settings))
        logger.info("Dask available in system: %s" % repr(dask_available))

    opath = Path(settings.get("output_name", None))
    if opath.exists() and not opath.is_file():
        raise IOError(
            "Output file %s already exists and cannot be overwritten! Aborting!"
            % (str(opath.resolve()))
        )
    dpath = Path(settings.get("dataset_path", None))
    if not dpath.is_file():
        raise IOError("Data file %s was not found! Aborting!" % str(dpath))
    dkey = settings.get("dataset_key", None)
    if dkey is not None:
        dkey = "/" + dkey if not dkey.startswith("/") else dkey

    ### Load data
    logger.info("Reading input data file")
    dims = settings.pop("dims", [])
    data_input = None
    if ".h5" in dpath.suffixes:
        if dkey is None:
            if qlknn_available:
                (data_input, f_outp, f_const) = load_from_store(str(dpath.resolve()))
                output_list = []
                for var in f_outp.columns:
                    if var in dims:
                        output_list.append(var)
                data_input = data_input.join(f_outp[output_list])
                if settings["debug_mode"]:
                    logger.warning("Using HDF5 data loader from qlknn package!")
            else:
                raise IOError("Key not provided for HDF5 data loader, aborting!")
        else:
            data_input = pd.read_hdf(str(dpath.resolve()), dkey)
        drop_list = []
        for var in data_input.columns:
            if var not in dims:
                drop_list.append(var)
        if len(drop_list) > 0:
            data_input = data_input.drop(drop_list, axis=1)
        finite_mask = np.isfinite(data_input.index)
        for key in data_input:
            finite_mask &= np.isfinite(data_input[key])
        data_input = data_input.loc[finite_mask]
        if "num_dim" not in settings:
            settings["num_dim"] = len(data_input.columns)
    elif ".nc" in dpath.suffixes:
        ds = xr.open_dataset(str(dpath.resolve()))
        coord_list = []
        index_list = []
        drop_list = []
        ds = ds.sel(nions=0)
        for var in ds.coords:
            if var in ds.indexes:
                index_list.append(var)
        if len(index_list) > 0:
            ds = ds.reset_index(index_list)
        for var in ds.coords:
            coord_list.append(var)
        if len(coord_list) > 0:
            ds = ds.reset_coords(coord_list)
        for var in ds.data_vars:
            if var not in dims:
                drop_list.append(var)
        if len(drop_list) > 0:
            ds = ds.drop_vars(drop_list)
        data_input = ds.to_dataframe()
        if "num_dim" not in settings:
            settings["num_dim"] = len(data_input.columns)
    else:
        logger.warning("This input format is not supported!")

    if data_input is None:
        raise ValueError("Input data not found or properly parsed! Aborting!")
    input_list = [str(key) for key in data_input.columns]

    if settings["debug_mode"]:
        logger.info("Columns targeted from reduction: %s" % (repr(input_list)))

    if settings["plot"] and "plot_variables" in settings:
        missing_vars = [var for var in plot_vars if var not in data_input.columns]
        if len(missing_vars) > 0:
            raise KeyError("Plot variables {} not found in given dataset".format(missing_vars))

    data_output = two_step_clustering(data_input, **settings)

    # logger.info("Start saving data to pickle")
    # df = pd.DataFrame(data_output, columns=settings['dims'])
    # df['iteration_counter'] = iteration_counter
    # pickle_name = settings.get("pickle_name", None)
    # df.to_pickle(pickle_name)
    # df['iteration_counter'] = df['iteration_counter'].astype(np.int64)

    # logger.info("Saving data to HDF5 file")
    # data_input[input_list].to_hdf(str(opath.resolve()), "input", format="table")
    # data_output[input_list].to_hdf(str(opath.resolve()), "flattened", format="table")
    # for name in data_output.columns:
    #     if name not in input_list:
    #         data_output[name].to_hdf(
    #             str(opath.resolve()), "output/" + name, format="table"
    #         )
    # store = pd.HDFStore(str(opath.resolve()))
    # store["constants"] = pd.DataFrame()
    # store.close()

    logger.info("Saving data to HDF5 file")
    data_input[input_list].to_hdf(str(opath.resolve()), "/original", format="table")
    data_output[input_list].to_hdf(str(opath.resolve()), "/reduced", format="table")

    logger.info("DBSCAN-kmeans reduction algorithm completed!")


def main():
    import argparse

    parser = argparse.ArgumentParser(description="Launch database clustering algorithm")
    parser.add_argument(
        "--settings", default=None, type=str, help="Optional settings JSON file"
    )
    parser.add_argument(
        "--verbose", "-v", action="count", default=0
    )
    parser.add_argument(
        "--debug", default=False, action="store_true", help="Toggle debug mode"
    )
    parser.add_argument(
        "--plot", default=False, action="store_true", help="Toggle 3D plots of identified clusters for each DBSCAN"
    )
    parser.add_argument(
        "--plot_variables", default=None, type=str, help="Comma separated string 'x,y,x' giving the 3 variables desired for plotting"
    )
    args = parser.parse_args()

    pvars = None
    if args.plot_variables is not None:
        pvars = args.plot_variables.split(',')
    run_clustering(args.settings, verbose=args.verbose, fdebug=args.debug, plot=args.plot, plot_vars=pvars)


if __name__ == "__main__":
    main()
