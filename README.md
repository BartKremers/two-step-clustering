# Two-step-clustering algorithm #

Implements data-density reduction, using the DBSCAN and k-means algorithms, as described in the Master thesis of B.J.J. Kremers (TU/e) (with some updates)

The package must first be installed using `pip install <path-to-repo>`, which will create an executable program called `two_step_clustering`

Example command line call:
```
two_step_clustering --settings=<path-to-settings-file> -vvvv
```

## Usage: ##

The `two_step_clustering()` method runs the full algorithm on a dataset, including the DBSCAN and k-means steps. This algorithm is as follows:
1. rescales data to unit variance and zero mean
2. initial DBSCAN to identify outliers (which are removed) and clusters (with initial_eps).
3. Clusters which pass size condition (min_cluster_size < size < RT, see explanation of
   settings below) are k-means reduced.
4. Then starts the reduction loop: in each iteration, the eps value is reduced by 0.05,
   DBSCAN is run to ID clusters, and these clusters are reduced if they pass the size
   condition. Exit conditions to this loop are:
   1. if DBSCAN finds no more clusters,
   2. if min_eps is reached,
   3. _or_ if that iteration reduced all the remaining clusters.

The `run_clustering()` method takes a path to a settings file, opens the requested dataset, and runs `two_step_clustering()` according to the settings. (Default settings are stored in the `example_settings.json` file). Possible settings fields are:

- `dims`: list of dataset dimensions to be included in the reduction, both inputs and outputs
- `dataset_path`: dataset to be reduced
- `dataset_key`: HDF5 key where dataset is store (only applicable when loading from an HDF5 file)
- `output_name`: where reduced results are stored
- `kmeans_dask`: whether to use dask memory management module
- `initial_eps`: initial radius passed to DBSCAN to determine clusters (default: 1)
- `min_eps`: stopping radius; reduction loop stops once this radius is reached (default: 0.3)
- `eps_step`: amount to step radius down by in each two-step-clustering iteration (default: 0.05)
- `min_samples`: DBSCAN parameter which determines which points are 'center points' of a cluster (default: 2)
- `min_cluster_size`: passed to 'run_dbscan' (default: 6)
- `reduction_threshold`: passed to 'run_dbscan' (default: 50)
- `min_reduction`: (default: 20)
- `zeta`: (default: 0)
- `tau`: (default: 1000)

The parameters `min_cluster_size` and `reduction_threshold` bound which clusters are flagged for k-means clustering in that iteration, i.e. cluster with size `min_cluster_size < size < reduction_threshold` are flagged.

The parameters `min_reduction`, `zeta`, and `tau` are used to calculate the k-value used in the k-means steps, i.e. how many points will replace a determined cluster. The calculation of this k-value is done in `k_expression_bart()` and its modular structure allows other functions to be tested / added in the future.
